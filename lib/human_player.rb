


class HumanPlayer
  attr_accessor :name, :mark

  def initialize
    @name = name
  end

  def get_move
    print "Please enter a move (ex. 0, 1): "
    move = gets.chomp

    until valid_move?(move)
      print "That's not a valid move! Please enter a move (ex. 0, 1): "
      move = gets.chomp
    end

    understand_move(move)
  end

  def display(board)
    @board = board
    puts
    puts
    puts board.to_s


  end


  private

  def valid_move?(move)
    move = understand_move(move)
    return false unless move.length == 2
    move.all? { |coord| coord.between?(0, 2) }
    @board.valid?(move)
  end

  def understand_move(move)
    move.split(",").map(&:to_i)
  end
end
