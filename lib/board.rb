

class Board

  attr_reader :grid

  def initialize(grid = false)
    @grid = grid || Array.new(3) { Array.new(3) }
  end

  def [](position)
    x, y = position
    @grid[x][y]
  end

  def []=(position, value)
    x, y = position
    @grid[x][y] = value
  end

  def place_mark(position, mark)
    self[position] = mark
  end

  def empty?(position)
    self[position].nil?
  end

  def winner
    [:X, :O].each { |mark| return mark if won?(mark) }
    nil
  end

  def to_s
    @grid.map do |row|
      row.map { |el| el.nil? ? "_" : el}.join(" ")
    end
  end





  def over?
    winner || @grid.flatten.count(nil) == 0
  end


  def empty_positions
    positions.select { |position| self.empty?(position)}
  end

  def will_win?(position, mark)
    place_mark(position, mark)
    will_win = winner == mark
    self[position] = nil
    will_win
  end

  def valid?(move)
    move.all? { |coord| coord.between?(0, 2) } && empty_positions.include?(move)

  end


  private

  def positions
    p = []
    3.times do |row|
      3.times { |column| p << [row, column] }
    end
    p

  end

  def won?(mark)
    line_wins.any? do |line|
      line.all? { |position| self[position] == mark }
    end
  end

  def line_wins
    horizontal = (0..2).map do |y|
      [[0, y], [1, y], [2, y]]
    end

    vertical = (0..2).map do |x|
      [[x, 0], [x, 1], [x, 2]]
    end

    diagonal = [
      [[0, 0], [1, 1], [2, 2]],
      [[0, 2], [1, 1], [2, 0]]
    ]

    horizontal + vertical + diagonal
  end
end
