class ComputerPlayer
  attr_accessor :name, :mark, :board

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end


  def get_move
    @board.empty_positions.each do |position|
      return position if @board.will_win?(position, @mark)
    end

    @board.empty_positions.sample

  end




end
